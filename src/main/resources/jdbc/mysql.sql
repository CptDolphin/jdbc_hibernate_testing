CREATE SCHEMA `jdbc` ;
use `jdbc` ;

CREATE TABLE `jdbc`.`person` (
  `id` INT NOT NULL AUTO_INCREMENT,
  `first_name` VARCHAR(255) NOT NULL,
  `last_name` VARCHAR(255) NOT NULL,
  PRIMARY KEY (`id`));

CREATE USER 'user_jdbc'@'localhost' IDENTIFIED BY 'jdbc01';

GRANT USAGE ON jdbc.* TO 'user_jdbc'@'localhost' identified BY 'jdbc01';
GRANT ALL PRIVILEGES ON jdbc.* TO 'user_jdbc'@'localhost';

FLUSH PRIVILEGES;

INSERT INTO person (first_name, last_name) VALUES ('firstname1', 'lastname1'), ('firstname2', 'lastname2');


CREATE TABLE `jdbc`.`animal` (
  `id` INT NOT NULL,
  `name` VARCHAR(45) NOT NULL,
  `type` VARCHAR(45) NOT NULL,
  `birthdate` DATE NOT NULL,
  `owner_id` INT NOT NULL,
  PRIMARY KEY (`id`),
  CONSTRAINT `owner_id`
    FOREIGN KEY (`id`)
    REFERENCES `jdbc`.`person` (`id`));