package pl.sda.hibernate;

import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.Transaction;
import pl.sda.hibernate.domain.Employee;
import pl.sda.hibernate.domain.User;
import pl.sda.hibernate.util.HibernateUtil;

public class HibernateDemo {

    public static void main(String[] args) {

        SessionFactory sessionFactory = null;
        Session session = null;
        Transaction transaction = null;

        try {
            sessionFactory = HibernateUtil.getSessionFactory();
            session = sessionFactory.openSession();
            transaction = session.beginTransaction();

//			create
//			Employee employee = new Employee("Rafal","tajneHaslo",21);
//			session.persist(employee);

//			read
            for (int i = 0; i < 10000; i++) {
                Employee employee = session.find(Employee.class, 2);
                System.out.println("employee = " + employee);
            }

//			update
//			employee.setPassword("asdasd");

//			delete
//			session.delete(employee);
//			System.out.println("employee = " + employee);

            transaction.commit();
        } catch (Exception e) {
            transaction.rollback();
        } finally {
            session.close();
            sessionFactory.close();
        }

    }
}
