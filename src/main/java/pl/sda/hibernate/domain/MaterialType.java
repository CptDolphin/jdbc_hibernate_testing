package pl.sda.hibernate.domain;

public enum MaterialType {
	BRICK,
	WOOD,
	METAL,
	GLASS,
	STONE;
}
