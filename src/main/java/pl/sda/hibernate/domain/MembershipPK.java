package pl.sda.hibernate.domain;

import javax.persistence.Embeddable;
import java.io.Serializable;

@Embeddable
public class MembershipPK implements Serializable {

	private int memberId;

	private int clubId;

	public MembershipPK() {
	}

	public MembershipPK(int memberId, int clubId) {
		this.memberId = memberId;
		this.clubId = clubId;
	}

	@Override
	public String toString() {
		return "MembershipPK{" +
				"memberId=" + memberId +
				", clubId=" + clubId +
				'}';
	}
}
