package pl.sda.jdbc.domain;

import java.sql.SQLException;
import java.util.Optional;

public class ExampleUpdate {

	public static void main(String[] args) throws SQLException {
		PersonDAO personDAO = new PersonDAO();

		Optional<Person> byId = personDAO.findById(3L);
		Person person = byId.get();
		System.out.println(person);
		person.setLastName("Kowalski");
		person.setFirstName("Jan");
		System.out.println(person);

//		personDAO.updatePerson(person);
		System.out.println(personDAO.findById(3L));
	}
}
