package pl.sda.jdbc.domain;

import java.sql.*;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

public class AnimalDao {
    private Connection connection;
    private PreparedStatement preparedStatement;
    private Statement statement;
    private ResultSet resultSet;

    public List<Animal> findAll() {
        List<Animal> list = new ArrayList<>();
        try {
            String query = "SELECT * FROM animal";
            preparedStatement = connection.prepareStatement(query);
            resultSet = preparedStatement.executeQuery();
            while(resultSet.next()){
                String name = resultSet.getString("name");
                String rasa = resultSet.getString("type");
                Date datebirth = resultSet.getDate("birthdate");
                int owner_id = resultSet.getInt("owner_id");
                list.add(new Animal(name,rasa,datebirth,owner_id));
            }
            return list;
        } catch (SQLException e) {
            e.printStackTrace();
        }
        return null;
    }

    public void addAnimal(String name, String rasa, String birthdate) {
        try {
            String query = "INSERT INTO animal VALUES(default,?,?,?);";
            preparedStatement = connection.prepareStatement(query);
            preparedStatement.setString(1, name);
            preparedStatement.setString(2, rasa);
            preparedStatement.setDate(3, java.sql.Date.valueOf(birthdate));
            preparedStatement.executeUpdate();
        } catch (SQLException e) {
            e.printStackTrace();
        }
    }

    public void addAniamlToPerson(int anima_id, int person_id) {

    }
}
