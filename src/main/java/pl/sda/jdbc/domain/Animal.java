package pl.sda.jdbc.domain;

import java.util.Date;

public class Animal {
    private String imie;
    private String rasa;
    private Date birthdate;
    private int owner_id;

    public Animal(String imie, String rasa, Date birthdate, int owner_id) {
        this.imie = imie;
        this.rasa = rasa;
        this.birthdate = birthdate;
        this.owner_id = owner_id;
    }

    @Override
    public String toString() {
        return "Animal{" +
                "imie='" + imie + '\'' +
                ", rasa='" + rasa + '\'' +
                ", birthdate=" + birthdate +
                ", owner_id=" + owner_id +
                '}';
    }

    public String getImie() {
        return imie;
    }

    public void setImie(String imie) {
        this.imie = imie;
    }

    public String getRasa() {
        return rasa;
    }

    public void setRasa(String rasa) {
        this.rasa = rasa;
    }

    public Date getBirthdate() {
        return birthdate;
    }

    public void setBirthdate(Date birthdate) {
        this.birthdate = birthdate;
    }

    public int getOwner_id() {
        return owner_id;
    }

    public void setOwner_id(int owner_id) {
        this.owner_id = owner_id;
    }
}
