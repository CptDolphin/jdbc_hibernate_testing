package pl.sda.jdbc.domain;


import pl.sda.jdbc.db.DBUtil;

import java.sql.*;
import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

public class PersonDAO {

    private final Connection connection;
    private Statement statement;
    private ResultSet resultSet;

    public PersonDAO() {
        this.connection = DBUtil.getConnection();
    }

    public Optional<Person> findById(Long personId) throws SQLException {
            statement = connection.createStatement();
            String query = "SELECT * FROM person where id="+personId;
            resultSet = statement.executeQuery(query);

            while (resultSet.next()) {
                long id = resultSet.getLong("id");
                String firstName = resultSet.getString("first_name");
                String lastName = resultSet.getString("last_name");
                Person person = new Person(id, firstName, lastName);
                return Optional.ofNullable(person);
            }
            return Optional.empty();
        }


    public List<Person> findAll() throws SQLException {
        try {
            statement = connection.createStatement();
            String query = "SELECT * FROM person";
            resultSet = statement.executeQuery(query);

            List<Person> people = new ArrayList<>();

            while (resultSet.next()) {
                long id = resultSet.getLong("id");
                String firstName = resultSet.getString("first_name");
                String lastName = resultSet.getString("last_name");
                people.add(new Person(id, firstName, lastName));
            }
            return people;
        } catch (Exception e) {
            e.printStackTrace();
        } finally {
            try {

                if (resultSet != null) {
                    resultSet.close();
                }
                if (statement != null) {
                    statement.close();
                }
            } catch (SQLException e) {
                e.printStackTrace();
            }
        }
        return null;
    }

    /**
     * A gdyby jednak zwracac nowoutworzony Person?
     * https://dev.mysql.com/doc/connector-j/5.1/en/connector-j-usagenotes-last-insert-id.html
     */
    public void addPerson(String firstName, String lastName) throws SQLException {
        try {
            String query = "INSERT INTO person VALUES (DEFAULT,?,?);";
            PreparedStatement statement = connection.prepareStatement(query);
            statement.setString(1, firstName);
            statement.setString(2, lastName);
            statement.executeUpdate();
        } catch (Exception e) {
            e.printStackTrace();
        } finally {
                if (resultSet != null) {
                    resultSet.close();
                }
                if (statement != null) {
                    statement.close();
                }
        }
    }

    public void updatePerson(int id, String first_name, String last_name) throws SQLException {
        try {
            String query = "UPDATE person SET first_name=?, last_name=? WHERE id=?";
            PreparedStatement statement = connection.prepareStatement(query);
            statement.setString(1,first_name);
            statement.setString(2,last_name);
            statement.setLong(3,id);
            int result = statement.executeUpdate();
            System.out.println(result);
        } catch (Exception e) {
            e.printStackTrace();
        } finally {
            try {
                if (resultSet != null) {
                    resultSet.close();
                }
                if (statement != null) {
                    statement.close();
                }
            } catch (SQLException e) {
                e.printStackTrace();
            }
        }
    }

    public void deletePerson(Long personId) throws SQLException {
        try{
            String query = "DELETE FROM person WHERE id=?";
            PreparedStatement statement = connection.prepareStatement(query);
            statement.setLong(1,personId);
            statement.executeUpdate();
        }catch (Exception e){
            e.printStackTrace();
        }
    }

}








