package pl.sda.jdbc.domain;


import pl.sda.jdbc.db.DBUtil;

import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.List;

public class ExampleQuery {

	public static void main(String[] args) {
		Connection connection = DBUtil.getConnection();
		Statement statement = null;
		ResultSet resultSet = null;
		try {
			statement = connection.createStatement();
			String query = "SELECT * FROM person";
			resultSet = statement.executeQuery(query);

			List<Person> people = new ArrayList<>();

			while (resultSet.next()) {
				long id = resultSet.getLong("id");
				String firstName = resultSet.getString("first_name");
				String lastName = resultSet.getString("last_name");
				Person person = new Person();
				person.setId(id);
				person.setFirstName(firstName);
				person.setLastName(lastName);

				people.add(person);
			}
			System.out.println(people);

		} catch (Exception e) {
			e.printStackTrace();
		} finally {
			try {
				if (resultSet != null) {
					resultSet.close();
				}
				if (statement != null) {
					statement.close();
				}
				connection.close();
			} catch (SQLException e) {
				e.printStackTrace();
			}
		}
	}
}
