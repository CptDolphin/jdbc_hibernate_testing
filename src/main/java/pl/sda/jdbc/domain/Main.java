package pl.sda.jdbc.domain;

import java.sql.SQLException;
import java.util.Scanner;

public class Main {
    public static void main(String[] args) throws SQLException {
        PersonDAO personDAO = new PersonDAO();
        AnimalDao animalDao = new AnimalDao();

        Scanner scanner = new Scanner(System.in);
        boolean isWorking = true;

        while (isWorking) {
            String line = scanner.nextLine();
            String[] splits = line.split(" ");
            if (splits[0].equalsIgnoreCase("findall")) {
                System.out.println(personDAO.findAll());
            } else if (splits[0].equalsIgnoreCase("findbyid")) {
                System.out.println(personDAO.findById(Long.valueOf(splits[1])));
            } else if(splits[0].equalsIgnoreCase("add")){
                personDAO.addPerson(splits[1],splits[2]);
            } else if(splits[0].equalsIgnoreCase("update")){
                personDAO.updatePerson(Integer.parseInt(splits[1]),splits[2],splits[3]);
            } else if(splits[0].equalsIgnoreCase("delete")){
                personDAO.deletePerson(Long.valueOf(splits[1]));
            }else if(splits[0].equalsIgnoreCase("addpet")){
                animalDao.addAnimal(splits[1],splits[2], splits[3]);
            }else if(splits[0].equalsIgnoreCase("findallpet")){
                System.out.println(animalDao.findAll());
            }else if(splits[0].equalsIgnoreCase("givepersonpet")){
                animalDao.addAniamlToPerson(Integer.parseInt(splits[1]),Integer.parseInt(splits[2]));
            }else if (line.equalsIgnoreCase("quit")) {
                isWorking = false;
            }
        }
    }
}
