package pl.sda.jdbc.exercise;

import java.util.Date;

public class Application {

	public static void main(String[] args) {
		Object[] tab = {1,"Rafal",new Date()};
		User user = new User((int) tab[0], (String) tab[1],(Date) tab[2]);
		System.out.println("user = " + user);
	}
}
