package pl.sda.jdbc.exercise;

import java.util.Date;

public class Task {

	private long id;                // id zadania
	private String title;           // tytul zadania
	private String category;        // kategoria zadania
	private double hours;           // pracochlonnosc
	private Date created;           // data utworzenia
	private Date modified;          // data modyfikacji
	private Integer[] dependsOn;    // idki zaleznych zadan
	private boolean done;           // czy jest zakonczone

}
